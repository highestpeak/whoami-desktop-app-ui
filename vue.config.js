const path = require('path')

function resolve(dir) {
    return path.join(__dirname, dir)
}

module.exports = {
    publicPath: './',
    devServer: {
        // can be overwritten by process.env.HOST
        host: '0.0.0.0',
        port: 8080
    },
    chainWebpack: config => {
        config.resolve.alias
            .set('modules',resolve('node_modules'))
            .set('@', resolve('src/renderprocess'))
            .set('src', resolve('src/renderprocess'))
            .set('assets', resolve('src/renderprocess/assets'))
            .set('components', resolve('src/renderprocess/components'))
            .set('main', resolve('src/mainprocess'))
    },
    pluginOptions: {
        electronBuilder: {
            builderOptions: {
                win: {
                    icon: './public/logo.png'
                },
                mac: {
                    icon: './public/logo.png'
                }
            }
        }
    }
}

