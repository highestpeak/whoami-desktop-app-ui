'use strict'
// 教程1 https://juejin.im/post/5d1abff7f265da1bb80c47e3
import { app, protocol, BrowserWindow, globalShortcut } from 'electron'
import { createProtocol } from 'vue-cli-plugin-electron-builder/lib'

import { createMenu } from './mainprocess/init.js'
const isDevelopment = process.env.NODE_ENV !== 'production'
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win

// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([
    { scheme: 'app', privileges: { secure: true, standard: true } }
])

app.allowRendererProcessReuse = true

function createWindow() {
    // Create the browser window.
    win = new BrowserWindow({
        width: 960,
        height: 630,
        title: 'WHOAMI',
        frame: false,
        webPreferences: {
            // Use pluginOptions.nodeIntegration, leave this alone
            // See nklayman.github.io/vue-cli-plugin-electron-builder/guide/security.html#node-integration for more info
            // 这个值如果为false 或者为下一行的值的话，在渲染进程即这里的vue重就不能使用require等函数，window.require也不能使用
            nodeIntegration: true,
            // nodeIntegration: process.env.ELECTRON_NODE_INTEGRATION,
            webSecurity: false,
        },
        icon: `${__static}/logo.png`
    })

    // 防止更新窗口标题
    win.on('page-title-updated', function (e) {
        e.preventDefault()
    });

    win.once('ready-to-show', () => {
        win.show()
    })

    if (process.env.WEBPACK_DEV_SERVER_URL) {
        // Load the url of the dev server if in development mode
        win.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
        if (!process.env.IS_TEST) win.webContents.openDevTools()
    } else {
        createProtocol('app')
        // Load the index.html when not in development
        win.loadURL('app://./index.html')
    }

    win.on('closed', () => {
        win = null
    })

    globalShortcut.register('CommandOrControl+Shift+i', function () {
        win.webContents.openDevTools()
    })

    // console.log('hhhh')
    createMenu()
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
        createWindow()
    }
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {
    createWindow()
})

app.whenReady().then(async () => {
    if (isDevelopment && !process.env.IS_TEST) {
        // Install Vue Devtools
        // try {
        //     const toolsPath = 'C:/Users/highestpeak/AppData/Local/Google/Chrome/User Data/Default/Extensions/nhdogjmejiglipccpnnnanhbledajbpd/5.3.3_0'
        //     await BrowserWindow.addDevToolsExtension(toolsPath)
        // } catch (e) {
        //     console.error('Vue Devtools failed to install:', e.toString())
        // }
    }

})

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
    if (process.platform === 'win32') {
        process.on('message', (data) => {
            if (data === 'graceful-exit') {
                app.quit()
            }
        })
    } else {
        process.on('SIGTERM', () => {
            app.quit()
        })
    }
}

const ipc = require('electron').ipcMain
ipc.on('close', e => mainWindow.close());
