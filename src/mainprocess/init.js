import { Menu } from 'electron'

// 设置菜单栏
function createMenu() {
    // darwin表示macOS，针对macOS的设置
    if (process.platform === 'darwin') {
        const template = [
            {
                label: 'App Demo',
                submenu: [
                    {
                        role: 'about'
                    },
                    {
                        role: 'quit'
                    }]
            }]
        const menu = Menu.buildFromTemplate(template)
        Menu.setApplicationMenu(menu)
    } else {
        // windows及linux系统
        Menu.setApplicationMenu(null)
    }
}
export {
    createMenu
}
