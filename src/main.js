// import vue type
import Vue from 'vue'

// import fontawesome
/*
// npm i --save @fortawesome/fontawesome-svg-core
// npm i --save @fortawesome/vue-fontawesome
// npm i --save @fortawesome/free-solid-svg-icons
// npm i --save @fortawesome/free-brands-svg-icons
// npm i --save @fortawesome/free-regular-svg-icons
*/
/*
// https://github.com/FortAwesome/vue-fontawesome
// 使用librabry可以减少最终打包的图标数量，并且可以命名别名，在更改的时候更为方便
// 下面这个连接说明了多种类别的图标的用法
// https://github.com/FortAwesome/vue-fontawesome#import-the-same-icon-from-different-styles
*/
import { library } from '@fortawesome/fontawesome-svg-core'
import { faHome, faSearch, faPlus, faCheck, faPlay, faPause, faTimes } from '@fortawesome/free-solid-svg-icons'
library.add(faHome, faSearch, faPlus, faCheck, faPlay, faPause, faTimes)
import { faMarkdown, faAppStore } from '@fortawesome/free-brands-svg-icons'
library.add(faMarkdown, faAppStore)
import { faFile } from '@fortawesome/free-regular-svg-icons'
library.add(faFile)
import { FontAwesomeIcon, FontAwesomeLayers } from '@fortawesome/vue-fontawesome'
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('font-awesome-layers', FontAwesomeLayers)

// import v-tooltip
// https://akryum.github.io/v-tooltip/#/
import VTooltip from 'v-tooltip'
Vue.use(VTooltip)

// import vue-multiselect
// https://vue-multiselect.js.org/
import Multiselect from 'vue-multiselect'
Vue.component('multiselect', Multiselect)

// https://github.com/bootstrap-vue/bootstrap-vue

// Vue.config
Vue.config.productionTip = false

// import vue instance
import App from './App.vue'
import TitleMenuBar from './renderprocess/components/menubar/TitleMenuBar.vue'
import SideMenuBar from './renderprocess/components/sidebar/SideMenuBar.vue'

// import main style css
import 'assets/css/main.styl';

// import router and store
import router from './renderprocess/router'
import store from './renderprocess/store'

// 可以创建多个实例
new Vue({
    router,
    store,
    render: h => h(TitleMenuBar)
}).$mount('#titleMenuBar')

new Vue({
    router,
    store,
    render: h => h(SideMenuBar)
}).$mount('#sideMenuBar')

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
