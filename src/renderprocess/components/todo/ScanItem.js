// https://github.com/pgilad/leasot

// attention: 在vue渲染进程中，只能使用Window对象来加载require
const fs = window.require('fs');
const leasot = window.require('leasot');
const path = window.require('path');
function test() {
    const mpath = "E:/_code/code_nodejs/SpiderFlight.java";

    const contents = fs.readFileSync(mpath, 'utf8');
    // get the filetype of the file, or force a special parser
    const filetype = '.java';
    // add file for better reporting
    const file = path.extname(mpath);
    const todos = leasot.parse(contents, { extension: filetype, filename: file });

    // -> todos now contains the array of todos/fixme parsed

    const output = leasot.report(todos, 'json', { spacing: 2 });

    console.log(output);
    // -> json output of the todos
}

// todo: 类jira的简易版本
// 

// 过程：
// 传入标签列表
// 传入扫描的文件集合/数组
// 扫描并返回

function scan(){

}

export {
    test
}
