import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        alias: ['/home', '/index'], // 路由别名
        name: 'Home',
        component: () => import('@/views/Home.vue')
    },
    {
        path: '/blog',
        name: 'Blog',
        component: () => import('@/views/Blog.vue')
    },
    {
        path: '/appstore',
        name: 'AppStore',
        component: () => import('@/views/AppStore.vue')
    }
]

const router = new VueRouter({
    routes
})

export default router
